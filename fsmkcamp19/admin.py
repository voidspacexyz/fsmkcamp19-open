from django.contrib import admin

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from fsmkcamp19.models import Registration, Speakers, Colleges, FAQ,Volunteers, Feedback

from fsmkcamp19.utils import send_html_email
class RegistrationResource(resources.ModelResource):

    class Meta:
        model = Registration
        fields = ["name","gender","college__name","track","contact","is_payment_received","transaction_id","is_confirmed","tshirt","why","created_at"]

def make_published(modeladmin, request, queryset):
    for obj in queryset:
        if obj.is_payment_received and obj.transaction_id:
            to_list = [obj.email]
            cost = 2000
            if obj.track == "iot":
                cost = 3800
            context = {"name":obj.name,"transaction_id": obj.transaction_id,"track":obj.get_track_display(),"fee":cost, "tshirt":obj.get_tshirt_display(),"college":obj.college.name,"volunteer":obj.college.volunteer,"coordinator":obj.college.coordinator}
            
            send_html_email(to_list, context, subject="FSMKCamps'19 Confirmation", template_name="register_confirm.html")
            obj.is_confirmed=True
            obj.save()

    
make_published.short_description = "Send confirmation email"

def send_reminder(modeladmin, request, queryset):
    for obj in queryset:
        if not obj.is_payment_received:
            to_list = [obj.email]
            cost = 2000
            if obj.track == "iot":
                cost = 3800
            context = {"name":obj.name,"transaction_id": obj.transaction_id,"track":obj.get_track_display(),"fee":cost, "tshirt":obj.get_tshirt_display(),"college":obj.college.name,"volunteer":obj.college.volunteer,"coordinator":obj.college.coordinator}
            
            send_html_email(to_list, context, subject="Pending Payment - FSMK Camp '19", template_name="register_reminder.html")

    
send_reminder.short_description = "Send reminder email"


class RegistrationAdmin(admin.ModelAdmin):
    resource_class = RegistrationResource
    list_display = ["name","gender","college","track","contact","hacktivist_camp"]
    list_filter = ["track","is_confirmed","gender","college__district","tshirt","hacktivist_camp"]
    search_fields = ["name","contact","transaction_id","email"]
    actions = [make_published,send_reminder]
    list_per_page = 400

class VolunteersAdmin(admin.ModelAdmin):
    list_display = ["name","gender","roomno","contact","college","is_arrived","hacktivist_camp"]
    list_filter = ["track","college","gender","tshirt","hacktivist_camp"]
    search_fields = ["name","contact"]

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ["feedback","fcategory","created_date"]
    list_filter = ["fcategory"]


admin.site.register(Registration,RegistrationAdmin)
admin.site.register(Speakers)
admin.site.register(Colleges)
admin.site.register(FAQ)
admin.site.register(Volunteers,VolunteersAdmin)
admin.site.register(Feedback,FeedbackAdmin)