from django import forms
from django.forms.widgets import HiddenInput

from fsmkcamp19.models import Registration,Feedback


class RegistrationForm(forms.ModelForm):

    class Meta:
        model = Registration
        fields = ["name","gender","college","contact","email","tshirt","track","why"]
        widgets = {
            'why': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            "track": forms.HiddenInput()
        }
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['why'].widget.attrs['placeholder'] = "What do you expect of the camp"
        self.fields['why'].label = "Expectations from camp"
        self.fields['college'].label = "College/State"
        self.fields['name'].widget.attrs['placeholder'] = self.fields['name'].help_text
        self.fields['contact'].widget.attrs['placeholder'] = self.fields['contact'].help_text
        self.fields['email'].widget.attrs['placeholder'] = self.fields['email'].help_text

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback 
        fields = ["feedback","fcategory"]


class DateInput(forms.DateInput):
    input_type = 'date'

class ArrivalDepartureForm(forms.ModelForm):

    class Meta:
        model = Registration
        fields = ["id","arrival","departure"]
        widgets = {
            'id': forms.Textarea(),
            'arrival': DateInput(),
            'departure': DateInput(),
        }