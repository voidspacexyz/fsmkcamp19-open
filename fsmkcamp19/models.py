from django.contrib.auth.models import User
from django.db import models

from fsmkcamp19.utils import send_html_email


gender_choices = (
    ("male","Male"),
    ("female","Female"),
    ("others","Others")
)
track_choices = (
    ("DSAI","Data science and Artificial Intelligence"),
    ("CC","Cloud computing and DevOps"),
    ("iot","Internet of Things")
)
tshirt_choices = (
    ("S","Small"),
    ("M","Medium"),
    ("L","Large"),
    ("XL","X-Large"),
    ("XXL","2X-Large")
)

hacktivist_camp_choices = (
        ("can","I am attending the hacktivist camp"),
        ("interested","I am intersted but cant attend the camp"),
        ("iffree","I can contribute if at all I am free"),
        ("notinterested","Not interested")
    )

class Speakers(models.Model):
    name = models.CharField(max_length=200)
    designation = models.CharField(max_length=200)
    description = models.TextField()
    facebook = models.CharField(max_length=100,blank=True,null=True)
    twitter = models.CharField(max_length=100,blank=True,null=True)
    mastodon = models.CharField(max_length=100,blank=True,null=True)
    instagram = models.CharField(max_length=100,blank=True,null=True)
    profile_pic = models.ImageField(upload_to='speakers/')

    def __str__(self):
        return self.name


class Colleges(models.Model):
    name = models.TextField()
    volunteer = models.ForeignKey(User, related_name="volunteer", on_delete = models.DO_NOTHING)
    coordinator = models.ForeignKey(User, related_name="coordinator", on_delete = models.DO_NOTHING)
    district = models.CharField(max_length = 100)


    def __str__(self):
        return self.name

        
class Registration(models.Model):

    name = models.CharField(max_length=200,help_text="Enter fullname as to be entered in certificate")
    gender = models.CharField(max_length=10,choices=gender_choices,default=gender_choices[1][0],help_text="Select your gender to allocate hostel rooms")
    college = models.ForeignKey(Colleges,on_delete=models.DO_NOTHING,default="Select college",help_text="Select your educational institute")
    track = models.CharField(max_length=10,choices=track_choices)
    contact = models.PositiveIntegerField(help_text="Contact number for communication and updates")
    email = models.EmailField(help_text="Email for official communication")
    why = models.TextField(help_text="Why do you want to attend this camp. The helps us understand why you are interested and does not determine your registration")
    transaction_id = models.TextField(blank=True,null=True)
    is_payment_received = models.BooleanField(default=False)
    is_confirmed = models.BooleanField(default=False)
    arrival = models.DateTimeField(blank=True,null=True)
    departure = models.DateTimeField(blank=True,null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    tshirt = models.CharField(max_length = 10, choices = tshirt_choices, default=tshirt_choices[0][0], help_text = "T-Shirt size that fits you")
    is_payment_verified = models.BooleanField(default=False)
    is_confirmation_email = models.BooleanField(default=False)
    roomno = models.CharField(max_length=100,blank=True,null=True,default="unassigned")
    is_arrived = models.BooleanField(default=False)
    hacktivist_camp = models.CharField(max_length=100, choices=hacktivist_camp_choices,default="notinterested")

class Volunteers(models.Model):
    name = models.CharField(max_length=200)
    college = models.ForeignKey(Colleges,on_delete=models.CASCADE)
    tshirt = models.CharField(max_length=20,choices=tshirt_choices)
    track = models.CharField(max_length=50,choices=track_choices)
    email = models.EmailField()
    contact = models.PositiveIntegerField()
    gender = models.CharField(max_length=20,choices=gender_choices)
    roomno = models.CharField(max_length=100,blank=True,null=True,default="unassigned")
    is_arrived = models.BooleanField(default=False)
    hacktivist_camp = models.CharField(max_length=100, choices=hacktivist_camp_choices,default="notinterested")

class FAQ(models.Model):
    question = models.CharField(max_length=500)
    answer = models.TextField()

    def __str__(self):
        return self.question

class Feedback(models.Model):
    feedback_choices = (
        ("iot","Internet of Things"),
        ("cc","Cloud computing"),
        ("ai","Data Science and Artificial Intelligence"),
        ("food","Food"),
        ("hostel","Hostel"),
        ("organising","Organising")
    )
    feedback = models.TextField()
    fcategory = models.CharField(max_length = 100, choices=feedback_choices)
    created_date = models.DateTimeField(auto_now_add=True)
