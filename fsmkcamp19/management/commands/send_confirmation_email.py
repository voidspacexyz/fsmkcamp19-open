from django.core.management.base import BaseCommand, CommandError
from fsmkcamp19.models import Registration, Volunteers
from fsmkcamp19.utils import send_html_email

class Command(BaseCommand):
    help = 'Sends instructions email '

    def handle(self, *args, **options):
        participants = Registration.objects.filter(is_confirmed=True, is_confirmation_email=False)
        for participant in participants:
            send_html_email([participant.email],{},subject="Instructions for FSMKCamp'19",template_name="instructions.html") 
            participant.is_confirmation_email = True 
            participant.save() 
        self.stdout.write(self.style.SUCCESS('Successfully sent email to %d people' % participants.count()))