# Generated by Django 2.2.dev20181224213600 on 2018-12-25 09:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Colleges',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('district', models.CharField(max_length=100)),
                ('volunteer', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Speakers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('designation', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('facebook', models.CharField(blank=True, max_length=100, null=True)),
                ('twitter', models.CharField(blank=True, max_length=100, null=True)),
                ('mastodon', models.CharField(blank=True, max_length=100, null=True)),
                ('instagram', models.CharField(blank=True, max_length=100, null=True)),
                ('profile_pic', models.ImageField(upload_to='speakers/')),
            ],
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Enter fullname as to be entered in certificate', max_length=200)),
                ('gender', models.CharField(choices=[('male', 'Male'), ('female', 'Female'), ('others', 'Others')], help_text='Select your gender to allocate hostel rooms', max_length=10)),
                ('track', models.CharField(choices=[('DSAI', 'Data science and Artificial Intelligence'), ('CC', 'Cloud computing and DevOps'), ('iot', 'Internet of Things')], max_length=10)),
                ('contact', models.PositiveIntegerField(help_text='Enter your contact mobile number')),
                ('email', models.EmailField(help_text='Email for official communication', max_length=254)),
                ('why', models.TextField(help_text='Why do you want to attend this camp. The helps us understand why you are interested and does not determine your registration')),
                ('transaction_id', models.TextField(blank=True, null=True)),
                ('is_payment_received', models.BooleanField(default=False)),
                ('is_confirmed', models.BooleanField(default=False)),
                ('arrival', models.DateTimeField(blank=True, null=True)),
                ('departure', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now=True)),
                ('tshirt', models.CharField(choices=[('S', 'Small'), ('M', 'Medium'), ('L', 'Large'), ('XL', 'X-Large'), ('XXL', '2X-Large')], help_text='T-Shirt size that fits you', max_length=10)),
                ('college', models.ForeignKey(help_text='Select your educational institute', on_delete=django.db.models.deletion.DO_NOTHING, to='fsmkcamp19.Colleges')),
            ],
        ),
    ]
